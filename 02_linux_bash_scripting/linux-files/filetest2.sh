#!/bin/bash
FILENAME=$1

if [ $# -eq 0 ];
then
	echo "no args supplied"
else
	for filename in "$@"
	do
		if [ ! -s $filename ];
		then
			echo "removing $filename"
			rm $filename
		else
			echo "inspected by for loop lab" >> $filename
		fi
	done	
fi
