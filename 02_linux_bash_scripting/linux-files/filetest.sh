#!/bin/bash
FILENAME=$1

if [ $# -eq 0 ];
then
	echo "no args supplied"
else
	if [ ! -f "$FILENAME" ];
	then
        	echo "file doesn't exist"
	fi
fi
