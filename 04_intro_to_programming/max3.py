def main():
    in1 = int(input("Enter a number: "))
    in2 = int(input("Enter another number: "))
    in3 = int(input("Enter a final number: "))
    print( max3(in1, in2, in3))
    list1 = [in1, in2, in3]
    print(sumnums(list1))
    print(remove_dups(list1))
    print("Is it a pangram?: ", is_pangram("abcdefghijklmnopqrstuvwxyz"))
    print("race car = race car? ",is_palindrome("race car"))
def max3(in1,in2,in3):
    return(max(in1,in2,in3))

def sumnums(ins):
    return sum(ins)


def remove_dups(ins):
    return list(set(ins))

def is_pangram(string):
    abcs = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w','x','y','z'}
    return abcs.issubset(set(string.lower()))

def is_palindrome(string):
    return (string.replace(" ","")[::-1]) == string.replace(" ","")
main()
