print("If you give me a year, I will notify you if it is a leap year.")
year = int(input("What year? "))

if year < 0:
    print("Invalid input")
else:    
    if year % 4 != 0:
        print("The year", year, "is not a leap year.")
    elif year % 100 != 0:
        print("The year", year, "is a leap year.")
    elif year % 400 != 0:
        print("The year", year, "is not a leap year")
    else:
        print("The year", year, "is a leap year.")

