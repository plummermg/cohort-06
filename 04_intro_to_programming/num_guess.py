# Mitch Plummer
#Cs021
#9/24/15
#Number guessing game

import random

#set constants
LOW = 1
HIGH = 100
MAX_TRIES = 5

play = True

def validate():
    guess = int(input("Enter a guess: "))
    if guess == 0:
        play = False
    while guess < LOW or guess > HIGH:
        print("Invalid Input!")
        guess = int(input("Enter a guess: "))
    return guess

while play == True:
    tries = 1
    num = random.randint(LOW,HIGH)
    print("I'm thinking of a number between 1 and 100")
    print("You have 5 tries to guess it. Good luck!")
    guess = validate()
        
    while guess != num and tries < MAX_TRIES:
        if guess > num:
            print("Too high!")
        else:
            print("Too low")
        guess = validate()
        tries += 1

    if guess == num:
        print("Congratulations! You guessed it in", tries, "tries!" )
    else:
        print("Sorry, you're out of tries. The number was", num)

    ask = input("Play again? (y or n): ")
    if ask == "y":
        play = True
    else:
        play = False
