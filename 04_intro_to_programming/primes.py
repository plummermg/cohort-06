HIGH = 30
LOW = 10

for x in range(LOW, HIGH+1):
    for y in range(2, x):
        if(x % y == 0):
            print(x, "not prime")
            break
        else:
            print(" ", x, "not divisible by ", y)
            if(y==(x-1)):
                print(x, "is prime")
