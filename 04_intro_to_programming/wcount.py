dic = dict()
filename = input('Enter a file name: ')
text = open(filename, 'r')

for line in text:
    line = line.strip()
    line = line.lower()
    words = line.split(" ")

    for word in words:
        word = word.strip(",?!.:;")
        if word in dic:
            dic[word] = dic[word] + 1
        else:
            dic[word] = 1

for word in sorted(dic, key=dic.get, reverse = True):
    print(word, ':', dic[word])


text.close()
