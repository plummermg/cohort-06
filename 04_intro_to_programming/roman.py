ROMAN = {'I':1,'V':5,'X':10,'L':50,'C':100,'D':500,'M':1000,
        'IV':4,'IX':9,'XL':40,'XC':90,'CD':400,'CM':900}
entry = input('Enter a roman numeral: ')
acc = 0
incr = 0
#for char in entry:
#    acc += ROMAN[char]

while incr < len(entry):
    if incr+1<len(entry) and entry[incr:incr+2] in ROMAN:
        acc += ROMAN[entry[incr:incr+2]]
        incr += 2
    else:
        acc += ROMAN[entry[incr]]
        incr += 1

print(acc)
