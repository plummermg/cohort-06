import sys

total = 0.0

for arg in sys.argv[1:]:
    try:
        arg = float(arg)
    except ValueError:
        print(f"There was a problem interpreting your argument {arg} as a number")
        print("... so I'm going to pretend like you didn't SAY that...")
        continue
    total = total + arg

print(total)
