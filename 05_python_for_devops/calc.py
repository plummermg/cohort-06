import math
def calculate(a, b, operator):
    if operator == '+':
        return a + b
    elif operator == '-':
        return a - b
    elif operator == '*':
        return a * b
    elif operator == 'log':
        try:            
            return math.log(a,b)
        except ValueError:
            return 'Your attempt created a math error'
        else:
            return "this shouldn't execute"

    elif operator == '/':
        try:
            return a / b
        except ZeroDivisionError:
            return ("You cannot divide by zero!")
    else:
        print('bad operator!', operator)
        return None
    
print(calculate(5,4,'*'))
print(calculate(2,0,'/'))
print(calculate(49.0,7,'log'))
print(calculate(49.0,-1,'log'))
