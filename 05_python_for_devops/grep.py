import re
import sys
def main():
    inarg = sys.argv
    try:
        file_handle(inarg[1], inarg[2])
    except IndexError:
        print('You need to provide the right arguments')
    except FileNotFoundError:
        print('that file doesn\'t exist')

def file_handle(fname, regex):
    for line in open(fname):
        if re.search(regex, line):
            print(line)
    return None


main()
