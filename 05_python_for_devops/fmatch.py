import fnmatch
import os

for f in os.listdir('.'):
    if fnmatch.fnmatch(f,'*.ipynb'):
        print(f,', Size: ', len(open(f).read()))
