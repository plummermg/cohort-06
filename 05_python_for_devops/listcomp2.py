colors = ['black','white']
sizes = ['s','m','l']
sleeves = ['short','long']

combos = [(color, size, sleeve)for color  in colors for size in sizes for sleeve in sleeves]

print(combos)
